import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import cors from "cors";

import { connection } from "./db.js";

import googleRouter from "./routes/google.js";
import lesssonRouter from "./routes/lesson.js";
import uploadRouter from "./routes/upload.js";
import levelRouter from "./routes/level.js";
import taskRouter from "./routes/task.js";

const app = express();
dotenv.config();
connection();
const PORT = process.env.PORT || 5000;

// mongoose.set("useFindAndModify", false);
// mongoose.set("useCreateIndex", true);
app.use(express.json({ limit: "30mb", extended: true }));
app.use(express.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());
app.use("/google", googleRouter);
app.use("/lesson", lesssonRouter);
app.use("/upload", uploadRouter);
app.use("/level", levelRouter);
app.use("/task", taskRouter);

// app.set("view engine", "ejs");
// app.get("/", (req, res) => {
//   res.send("SERVER IS RUNNING");
// });

app.get("/hello", (req, res) => {
  res.send("HELLO");
});

app.listen(PORT, console.log(`Listening on port ${PORT}...`));
