/**
 * Generates SSML text from plaintext.
 */
import { readFileSync } from "fs";
export const textToSsml = (inputFile) => {
  let rawLines = "";
  // Read input file
  try {
    rawLines = readFileSync(inputFile, "utf8");
  } catch (e) {
    console.log("Error:", e.stack);
    return;
  }

  // Replace special characters with HTML Ampersand Character Codes
  let escapedLines = rawLines;
  escapedLines = escapedLines.replace(/&/g, "&amp;");
  escapedLines = escapedLines.replace(/"/g, "&quot;");
  escapedLines = escapedLines.replace(/</g, "&lt;");
  escapedLines = escapedLines.replace(/>/g, "&gt;");

  // Convert plaintext to SSML
  const expandedNewline = escapedLines.replace(/\n/g, '\n<break time="2s"/>');
  const ssml = "<speak>" + expandedNewline + "</speak>";

  return ssml;
};
