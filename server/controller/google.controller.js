// Imports the Google Cloud client library
import { TextToSpeechClient } from "@google-cloud/text-to-speech";
import dotenv from "dotenv";
import { promisify } from "util";
import { writeFile } from "fs";
import { textToSsml } from "./converttossml.js";
import * as fs from "fs";
import * as util from "util";

dotenv.config();

// Creates a client
const client = new TextToSpeechClient();
export const txtToSpeech = async (req, res) => {
  // The text to synthesize
  const text = textToSsml(
    "C:/Users/HoneyBadger/Desktop/grad project/proj/Resources/Texts/l1_l1_t4_q1_m_writing.txt"
  );
  console.log(text);
  // Construct the request
  const request = {
    input: { ssml: text },
    // Select the language and SSML voice gender (optional)
    voice: { languageCode: "en-US", ssmlGender: "NEUTRAL" },
    // select the type of audio encoding
    audioConfig: { audioEncoding: "MP3", speakingRate: 0.75 },
  };

  // Performs the text-to-speech request
  const [response] = await client.synthesizeSpeech(request);
  // Write the binary audio content to a local file
  const writefile = util.promisify(fs.writeFile);
  await writeFile(
    "C:/Users/HoneyBadger/Desktop/grad project/proj/Resources/l1_l1_t4_q1_m_writing.mp3",
    response.audioContent,
    "binary",
    function (err) {
      if (err) throw err;
    }
  );
  res.send(text);
};
