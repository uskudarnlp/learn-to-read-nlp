import express from "express";
import Level from "../models/level.js";

export const createLevel = async (req, res) => {
  const level = new Level(req.body);
  try {
    await level.save();
    res.status(201).json(level);
  } catch (error) {
    res.status(409).json(error.level);
  }
};

export const getLevels = async (req, res) => {
  try {
    const levels = await Level.find({});
    res.status(201).json(levels);
  } catch (error) {
    res.status(409).json(error.levels);
  }
};
export const updateLevel = async (req, res) => {
  try {
    const level = await Level.findByIdAndUpdate(req.params.id, req.body);
    res.status(201).json(level);
  } catch (error) {
    res.status(409).json(error.level);
  }
};
