import express from "express";
import mongoose from "mongoose";
import Grid from "gridfs-stream";
import { connection } from "../db.js";

let gfs, gridfsBucket;

const conn = mongoose.connection;
conn.once("open", () => {
  // Add this line in the code
  gridfsBucket = new mongoose.mongo.GridFSBucket(conn.db, {
    bucketName: "files",
  });
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection("files");
});
export const uploadFile = async (req, res) => {
  console.log(req.file);
  if (req.file === undefined) return res.send("you must select a file.");
  const imgUrl = `http://localhost:5000/upload/${req.file.filename}`;
  return res.send(imgUrl);
};

export const getFile = async (req, res) => {
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
    // Check if file
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: "No file exists",
      });
    }

    // Check if image
    if (
      file.contentType === "image/jpeg" ||
      file.contentType === "image/png" ||
      file.contentType === "audio/mpeg"
    ) {
      // Read output to browser
      const readStream = gridfsBucket.openDownloadStream(file._id);
      readStream.pipe(res);
    } else {
      res.status(404).json({
        err: "Not an image/audio",
      });
    }
  });
};

/** to be fixed later */
export const deleteFile = async (req, res) => {};
