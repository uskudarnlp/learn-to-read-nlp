import express from "express";
import Task from "../models/task.js";

export const createTask = async (req, res) => {
  const task = new Task(req.body);
  try {
    await task.save();
    res.status(201).json(task);
  } catch (error) {
    res.status(409).json(error.task);
  }
};

export const getTasks = async (req, res) => {
  try {
    const tasks = await Task.find({});
    res.status(201).json(tasks);
  } catch (error) {
    res.status(409).json(error.tasks);
  }
};

export const getTask = async (req, res) => {
  try {
    const tasks = await Task.findById(req.params.id);
    res.status(201).json(tasks);
  } catch (error) {
    res.status(409).json(error.tasks);
  }
};

export const updateTask = async (req, res) => {
  try {
    const lesson = await Task.findByIdAndUpdate(req.params.id, req.body);
    res.status(201).json(lesson);
  } catch (error) {
    res.status(409).json(error.lesson);
  }
};
