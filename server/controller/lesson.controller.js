import express from "express";
import Lesson from "../models/lesson.js";
import Task from "../models/task.js";

import { connection } from "../db.js";

export const createLesson = async (req, res) => {
  const lesson = new Lesson(req.body);
  try {
    await lesson.save();
    res.status(201).json(lesson);
  } catch (error) {
    res.status(409).json(error.lesson);
  }
};

export const getLessons = async (req, res) => {
  try {
    // const lessons = await Lesson.find({});
    Lesson.aggregate([
      {
        $lookup: {
          from: Task.collection.name,
          localField: "tasks",
          foreignField: "_id",
          as: "tasks",
        },
      },
    ]).exec(function (err, lessons) {
      res.status(201).json(lessons);
    });
  } catch (error) {
    res.status(409).json(error.lessons);
  }
};

export const getLesson = async (req, res) => {
  try {
    await Lesson.aggregate([
      {
        $match: {
          $expr: { $eq: ["$_id", { $toObjectId: req.params.id }] },
        },
      },
      {
        $lookup: {
          from: Task.collection.name,
          localField: "tasks",
          foreignField: "_id",
          as: "tasks",
        },
      },
    ]).exec(function (err, lesson) {
      res.status(201).json(lesson);
    });
  } catch (error) {
    res.status(409).json(error.lesson);
  }
};

export const updateLesson = async (req, res) => {
  try {
    const lesson = await Lesson.findByIdAndUpdate(req.params.id, req.body);
    res.status(201).json(lesson);
  } catch (error) {
    res.status(409).json(error.lesson);
  }
};
