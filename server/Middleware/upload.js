import multer from "multer";
import GridfsStorage from "multer-gridfs-storage";

const storage = new GridfsStorage.GridFsStorage({
  url: process.env.DB_URL,
  options: { useNewUrlParser: true, useUnifiedTopology: true },
  file: (req, file) => {
    const match = ["image/png", "image/jpeg", "audio/mpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      const filename = `${Date.now()}-nlp-learning-file-${file.originalname}`;
      return filename;
    }

    return {
      bucketName: "files",
      filename: `${Date.now()}-nlp-learning-file-${file.originalname}`,
    };
  },
});

export default multer({ storage });
