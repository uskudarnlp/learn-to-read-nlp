import mongoose from "mongoose";

const TaskSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  number: Number,
  name: String,
  questions: [
    {
      number: Number,
      image: String,
      audio: String,
      video: String,
    },
  ],
  lesson: { type: mongoose.Schema.Types.ObjectId, ref: "Lesson" },
});

export default mongoose.models.Task || mongoose.model("Task", TaskSchema);
