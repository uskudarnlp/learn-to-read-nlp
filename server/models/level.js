import mongoose from "mongoose";

const LevelSchema = new mongoose.Schema({
  levelNumber: Number,
  lessons: [{ type: mongoose.Schema.Types.ObjectId, ref: "Lesson" }],
});

export default mongoose.models.Level || mongoose.model("Level", LevelSchema);
