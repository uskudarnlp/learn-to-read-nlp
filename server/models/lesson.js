import mongoose from "mongoose";

const LessonSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  lessonNumber: Number,
  levelNumber: Number,
  tasks: [{ type: mongoose.Schema.Types.ObjectId, ref: "Task" }],
});

export default mongoose.models.Lesson || mongoose.model("Lesson", LessonSchema);
