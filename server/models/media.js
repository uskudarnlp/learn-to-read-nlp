import mongoose from "mongoose";

const MediaSchema = new mongoose.Schema({
  name: String,
  desc: String,
  img: {
    data: Buffer,
    contentType: String,
  },
});

export default mongoose.models.Media || mongoose.model("Media", MediaSchema);
