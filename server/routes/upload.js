import express from "express";
import upload from "../Middleware/upload.js";
import {
  uploadFile,
  getFile,
  deleteFile,
} from "../controller/upload.controller.js";

const router = express.Router();

router.post("/", upload.single("file"), uploadFile);
router.get("/:filename", getFile);
router.delete("/:filename", deleteFile);

export default router;
