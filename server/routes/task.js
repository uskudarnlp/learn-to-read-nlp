import express from "express";
import {
  createTask,
  getTasks,
  getTask,
  updateTask,
} from "../controller/task.controller.js";
import { connection } from "../db.js";

await connection();
const router = express.Router();

router.post("/", createTask);
router.get("/", getTasks);
router.get("/:id", getTask);
router.put("/:id", updateTask);

export default router;
