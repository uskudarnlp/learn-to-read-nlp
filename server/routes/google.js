import express from "express";
import { txtToSpeech } from "../controller/google.controller.js";

const router = express.Router();

router.get("/", txtToSpeech);

export default router;
