import express from "express";
import {
  createLesson,
  getLessons,
  getLesson,
  updateLesson,
} from "../controller/lesson.controller.js";

const router = express.Router();

router.post("/", createLesson);
router.get("/", getLessons);
router.get("/:id", getLesson);
router.put("/:id", updateLesson);

export default router;
