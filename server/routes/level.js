import express from "express";
import {
  createLevel,
  getLevels,
  updateLevel,
} from "../controller/level.controller.js";

const router = express.Router();

router.post("/", createLevel);
router.get("/", getLevels);
router.put("/:id", updateLevel);

export default router;
