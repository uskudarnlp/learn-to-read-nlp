import React from "react";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div>
      <div className="flex h-screen">
        <div className="m-auto z-10">
          <div>
            <h1 class="text-8xl font-bold tracking-wide text-gray-900 dark:text-white md:text-5xl lg:text-8xl ">
              <span class="text-transparent bg-clip-text bg-gradient-to-r to-sky-300 from-indigo-600">
                Welcome!
              </span>
            </h1>
          </div>
          <div className="place-items-center">
            <div class="mb-6 ml-1 text-lg text-cyan-800 lg:text-xl font-medium">
              Learning made fun and easy.
            </div>
          </div>

          <div className="grid place-items-center">
            <Link
              to="/levels"
              class="inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800  rounded-lg"
            >
              Let's Start
              <svg
                class="ml-2 -mr-1 w-5 h-5"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                  clip-rule="evenodd"
                ></path>
              </svg>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
