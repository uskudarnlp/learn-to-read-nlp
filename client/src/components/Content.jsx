import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { useEffect, useState } from "react";

function Content() {
  const [levels, setLevels] = useState([]);

  const asyncRequestLevels = async () => {
    console.log("FFF");
    await axios({
      method: "get",
      url: "level",
    }).then((res) => {
      setLevels(res.data);
    });
  };
  useEffect(() => {
    asyncRequestLevels();
  }, []);

  return (
    <div>
      <div className="flex h-screen">
        <div className="m-auto z-10">
          <div className="items-center justify-center p-10 space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
            {levels.map((level) => (
              <div class="max-w-xs bg-white border border-gray-200 rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
                <img
                  class="rounded-t-lg"
                  src={require(`../resources/tasksimgs/${level.levelNumber}.png`)}
                  alt=""
                />
                <div class="p-5">
                  <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Level {level.levelNumber}
                  </h5>
                  <Link
                    to={{
                      pathname: `/level${level.levelNumber}/lessons`,
                    }}
                    state={{ level: level }}
                    class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg"
                  >
                    Start The Journey
                    <svg
                      aria-hidden="true"
                      class="w-4 h-4 ml-2 -mr-1"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Content;
