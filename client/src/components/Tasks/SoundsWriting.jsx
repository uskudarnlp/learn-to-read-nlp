import React from "react";
import { useLocation } from "react-router-dom";
import { useState, useEffect, useRef } from "react";
import LoadingState from "../LoadingState.jsx";
import CanvasDraw from "react-canvas-draw";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowLeft,
  faArrowRight,
  faPause,
  faPlay,
} from "@fortawesome/free-solid-svg-icons";

function SoundsWriting() {
  const location = useLocation();
  const task = location.state.task;
  const [loading, setLoading] = useState(false);
  const [questionNumber, setQuestionNumber] = useState(0);
  let imageUrl = task.questions[questionNumber].image;
  const [isPlaying, setIsPlaynig] = useState(false);
  const [instructionsAudio, setInstructionAudio] = useState();
  const [progressNumber, setProgressNumber] = useState();
  const [progressBar, setProgressBarStyle] = useState();
  const canvas = useRef();

  const progressBarStyle = {
    width: `${progressBar}%`,
  };

  const startfirstTask = async () => {
    setIsPlaynig(true);
    instructionsAudio.play();
    instructionsAudio.onended = () => {
      setIsPlaynig(false);
    };
  };

  useEffect(() => {
    setLoading(true);
    setProgressNumber(100 / task.questions.length);
    setProgressBarStyle(100 / task.questions.length);
    setInstructionAudio(new Audio(task.questions[0].audio));
    setLoading(false);
  }, []);

  const nextTask = () => {
    if (progressBar < 100) setProgressBarStyle(progressBar + progressNumber);
    if (questionNumber < task.questions.length - 1) {
      setQuestionNumber(questionNumber + 1);
      setInstructionAudio(new Audio(task.questions[questionNumber].audio));
    }
  };
  const previoustTask = () => {
    if (progressBar > progressNumber)
      setProgressBarStyle(progressBar - progressNumber);
    if (questionNumber > 0) {
      setQuestionNumber(questionNumber - 1);
      setInstructionAudio(new Audio(task.questions[questionNumber].audio));
    }
  };

  return (
    <div>
      <div className="grid grid-cols-2  h-screen place-items-center opacity-100 z-10 backdrop-blur-sm ">
        <LoadingState open={loading} />

        <div className="place-items-center relative  box-content w-full ml-44 h-4/5 mt-20 border-4 bg-white/30">
          <div class="flex items-center w-full bg-gray-200 h-3 dark:bg-gray-700">
            <div class="bg-cyan-600 h-3 " style={progressBarStyle}>
              {/* <span className="flex items-center justify-center h-2.5 text-white">
                {Math.trunc(progressBar)}%
              </span> */}
            </div>
          </div>
          <div className="absolute top-5 left-5">
            <button
              type="button"
              class="items-center justify-center  text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-22"
              onClick={previoustTask}
            >
              <FontAwesomeIcon icon={faArrowLeft} />
            </button>
          </div>
          <div className="absolute top-5 right-5">
            <button
              type="button"
              class="items-center justify-center  text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
              onClick={nextTask}
            >
              <FontAwesomeIcon icon={faArrowRight} />
            </button>
          </div>
          <div className="grid place-items-center pt-2">
            <button
              type="button"
              class="items-center justify-center  text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
              onClick={startfirstTask}
            >
              {" "}
              {!isPlaying ? (
                <FontAwesomeIcon icon={faPlay} />
              ) : (
                <FontAwesomeIcon icon={faPause} />
              )}
            </button>

            <div className="grid place-items-center mt-10">
              <img
                src={imageUrl}
                alt=""
                className="items-center justify-center "
              />
            </div>
            <div className=" mt-10">
              <iframe
                src={task.questions[questionNumber].video}
                width="400"
                height="250"
                allow="autoplay"
                title="vis"
              ></iframe>
            </div>
          </div>
        </div>
        <div className="grid h-screen place-items-center border-4 h-3/5 bg-white/30">
          <div>
            <CanvasDraw brushRadius={5} ref={canvas} />
          </div>
          <div>
            {" "}
            <button
              type="button"
              class="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br  font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2 mb-2"
              onClick={() => canvas.current.undo()}
            >
              Undo
            </button>
            <button
              type="button"
              class="text-white bg-gradient-to-r from-teal-400 via-teal-500 to-teal-600 hover:bg-gradient-to-br  font-medium rounded-lg text-sm px-2 py-1.5 text-center mr-2 mb-2"
              onClick={() => canvas.current.clear()}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SoundsWriting;
