import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowLeft,
  faArrowRight,
  faCircle,
  faPause,
  faPlay,
} from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import { useRef } from "react";
import { useLocation } from "react-router-dom";
import axios from "axios";
import LoadingState from "../LoadingState";

function SayItFast() {
  const location = useLocation();
  const task = location.state.task;
  const [loading, setLoading] = useState(false);
  const [questionNumber, setQuestionNumber] = useState(0);
  const [pingState, setPingState] = useState("hidden");
  let imageUrl = task.questions[questionNumber].image;
  let instructionsAudio = new Audio(task.questions[questionNumber].audio);

  const [progressNumber, setProgressNumber] = useState();
  const [progressBar, setProgressBarStyle] = useState();
  const [isPlaying, setIsPlaynig] = useState(false);

  useEffect(() => {
    setLoading(true);
    setProgressNumber(100 / task.questions.length);
    setProgressBarStyle(100 / task.questions.length);
    //setInstructionAudio(new Audio(task.questions[questionNumber].audio));
    setLoading(false);
  }, []);

  const progressBarStyle = {
    width: `${progressBar}%`,
  };

  const nextTask = () => {
    setPingState("hidden");
    if (progressBar < 100) setProgressBarStyle(progressBar + progressNumber);
    if (questionNumber < task.questions.length - 1) {
      setQuestionNumber(questionNumber + 1);
    }
  };
  const previoustTask = () => {
    setPingState("hidden");
    if (progressBar > progressNumber)
      setProgressBarStyle(progressBar - progressNumber);
    if (questionNumber > 0) {
      setQuestionNumber(questionNumber - 1);
    }
  };

  const startTask = async () => {
    setIsPlaynig(true);
    instructionsAudio.play();
    instructionsAudio.onended = () => {
      setIsPlaynig(false);
      setPingState("");
    };
  };

  return (
    <div>
      {task && (
        <div className="grid h-screen place-items-center opacity-100 z-10 backdrop-blur-sm ">
          <LoadingState open={loading} />

          <div className="flex z-20 place-items-center  box-content border-4 bg-white/30">
            TASK {task.number} : {task.name} | Exercise{" "}
            {task.questions[questionNumber].number}
          </div>

          <div className="place-items-center relative  box-content w-1/2 border-4 bg-white/30">
            <div class="flex items-center w-full bg-gray-200 h-3 dark:bg-gray-700">
              <div class="bg-cyan-600 h-3 " style={progressBarStyle}>
                {/* <span className="flex items-center justify-center h-2.5 text-white">
                {Math.trunc(progressBar)}%
              </span> */}
              </div>
            </div>
            <div className="absolute top-5 left-5">
              <button
                type="button"
                class="items-center justify-center  text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-22"
                onClick={previoustTask}
              >
                <FontAwesomeIcon icon={faArrowLeft} />
              </button>
            </div>
            <div className="absolute top-5 right-5">
              <button
                type="button"
                class="items-center justify-center  text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                onClick={nextTask}
              >
                <FontAwesomeIcon icon={faArrowRight} />
              </button>
            </div>
            <div className="grid place-items-center pt-2">
              <button
                type="button"
                class="items-center justify-center  text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                onClick={startTask}
              >
                {!isPlaying ? (
                  <FontAwesomeIcon icon={faPlay} />
                ) : (
                  <FontAwesomeIcon icon={faPause} />
                )}
              </button>
            </div>

            <div className="grid place-items-center pt-10">
              <img
                src={imageUrl}
                alt=""
                className="items-center justify-center "
              />
            </div>
            <div className=" grid place-items-center py-10">
              <div className={`absolute ${pingState}`}>
                <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-green-600 opacity-75"></span>
                <span class="text-white relative bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                  Your Turn
                </span>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default SayItFast;
