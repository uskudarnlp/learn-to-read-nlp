import React from "react";

function Error() {
  return (
    <div>
      <div className="bg-gray-50 flex h-screen">
        <div className="m-auto z-10">
          <div className=" text-9xl font-extrabold">404</div>
          <div className="text-4xl font-medium text-gray-900 dark:text-white">
            OOPS! The requested page could not be found
          </div>
          <a
            href="/"
            class="font-medium text-blue-600 dark:text-blue-500 hover:underline"
          >
            Go back to home page
          </a>
        </div>
      </div>
    </div>
  );
}

export default Error;
