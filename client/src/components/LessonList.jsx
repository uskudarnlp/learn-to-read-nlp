import React from "react";
import { Link, useLocation } from "react-router-dom";
import { useState } from "react";

function LessonList() {
  const location = useLocation();
  const level = location.state.level;
  const [lessons, setLessons] = useState(level.lessons);
  console.log(lessons);

  return (
    <div>
      <div className="flex h-screen">
        <div className="m-auto z-10 ">
          <div className="flex  items-center justify-center  text-5xl mt-10 text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg  px-5 py-2.5 text-center  mb-2">
            LIST OF LESSONS
          </div>
          <div className="ml-2 mt-10">
            <div className="items-center justify-center grid grid-cols-5 gap-4">
              {lessons.map((id, index) => (
                <Link
                  to={`/level${level.levelNumber}/lesson${index + 1}/taskList`}
                  state={{ lessonId: id }}
                >
                  <div class=" rounded-full flex  items-center justify-center font-bold text-xl text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg  px-5 py-2.5 text-center mr-5 mb-5">
                    {index + 1}
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

// const items = (x, y) => {
//   return (
//     <div>
//       <div className="items-center justify-center grid grid-cols-5 gap-4">
//         {range(x, y).map((number) => (
//           <Link to={`/tasks/${number}`}>
//             <div class=" rounded-full flex  items-center justify-center font-bold text-xl text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg  px-5 py-2.5 text-center mr-5 mb-5">
//               {number}
//             </div>
//           </Link>
//         ))}
//       </div>
//     </div>
//   );
// };

export default LessonList;
