import React from "react";
import { Link, useLocation } from "react-router-dom";
import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";

function TaskList() {
  const location = useLocation();
  const [lesson, setLesson] = useState([]);
  const [tasks, setTasks] = useState([]);
  const lessonId = location.state.lessonId;

  const asyncRequestTasks = async () => {
    await axios({
      method: "get",
      baseURL: `http://localhost:3000/lesson/${lessonId}`,
    }).then((res) => {
      setLesson(res.data[0]);
      setTasks(res.data[0].tasks);
    });
  };
  useEffect(() => {
    asyncRequestTasks();
  }, []);

  return (
    <div>
      <div className="flex h-screen">
        <div className="m-auto z-10 ">
          <div className="flex  items-center justify-center  text-5xl mt-10 text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg  px-5 py-2.5 text-center  mb-2">
            LIST OF TASKS
          </div>
          <div className="ml-2 mt-10">
            {tasks && (
              <div className="items-center justify-center grid grid-rows-5 gap-4">
                {tasks.map((task, index) => (
                  <Link
                    to={`/level${lesson.levelNumber}/lesson${
                      lesson.lessonNumber
                    }/task${index + 1}/${task.name}`}
                    state={{ task: task }}
                  >
                    <div class=" rounded-full flex  items-center justify-center font-bold text-xl text-white bg-gradient-to-r from-cyan-400 via-cyan-500 to-cyan-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 shadow-lg shadow-cyan-500/50 dark:shadow-lg dark:shadow-cyan-800/80 font-medium rounded-lg  px-5 py-2.5 text-center mr-5 mb-5">
                      {task.number} : {task.name}
                    </div>
                  </Link>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default TaskList;
