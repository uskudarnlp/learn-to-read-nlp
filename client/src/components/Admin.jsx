import React, { useState, useEffect } from "react";
import mongoose from "mongoose";
import Button from "@mui/material/Button";
import axios from "axios";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Fab,
  TextField,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import LoadingState from "./LoadingState.jsx";

function Admin() {
  const [image, setImage] = useState({ preview: "", data: "" });
  const [audio, setAudio] = useState({ preview: "", data: "" });
  const [lessonDlg, setLessonDlg] = useState(false);
  const [taskDlg, setTaskDlg] = useState(false);
  const [loading, setLoading] = useState(false);
  const [lessonsDB, setLessonDB] = useState([]);
  const [levelsDB, setLevelsDB] = useState([]);
  const [tasks, setTasks] = useState([]);

  const asyncRequestLesson = async () => {
    try {
      const { data } = await axios({
        method: "get",
        url: "/lesson",
      });
      setLessonDB(data);
      setLoading(false);
    } catch (e) {
      console.error(e);
      setLoading(false);
    }
  };
  const asyncRequestLevel = async () => {
    try {
      const { data } = await axios({
        method: "get",
        url: "/level",
      });
      setLevelsDB(data);
      setLoading(false);
    } catch (e) {
      console.error(e);
      setLoading(false);
    }
  };
  const asyncRequestTasks = async () => {
    try {
      const { data } = await axios({
        method: "get",
        url: "/task",
      });
      setLessonDB(data);
      setLoading(false);
    } catch (e) {
      console.error(e);
      setLoading(false);
    }
  };
  useEffect(() => {
    setLoading(true);
    asyncRequestLevel();
    asyncRequestLesson();
  }, []);

  const handleImgChange = (e) => {
    const img = {
      preview: URL.createObjectURL(e.target.files[0]),
      data: e.target.files[0],
    };
    setImage(img);
  };
  const handleAudioChange = (e) => {
    const audio = {
      preview: URL.createObjectURL(e.target.files[0]),
      data: e.target.files[0],
    };
    setAudio(audio);
  };
  const save = async (e) => {
    e.preventDefault();
    setLoading(true);
    const questionNumber = e.target.question.value;
    const videoUrl = e.target.video_url.value;
    const task = tasks[e.target.task.value];

    let audioURL;
    let imgURL;

    //image saving
    let formDataImg = new FormData();
    formDataImg.append("file", image.data);
    await axios({
      method: "post",
      url: "/upload",
      data: formDataImg,
    }).then((res) => {
      imgURL = res.data;
    });
    // Audio saving
    let formDataAudio = new FormData();
    formDataAudio.append("file", audio.data);
    await axios({
      method: "post",
      url: "/upload",
      data: formDataAudio,
    }).then((res) => {
      audioURL = res.data;
    });

    // Question saving
    const questionData = {
      number: questionNumber,
      image: imgURL,
      audio: audioURL,
      video: videoUrl,
    };
    task.questions.push(questionData);
    console.log(task);
    await axios({
      method: "put",
      url: `/task/${task._id}`,
      data: task,
    }).then((res) => {
      console.log(res);
    });

    setLoading(false);
  };

  const addNewLesson = async (e) => {
    e.preventDefault();
    setLessonDlg(false);
    setLoading(true);
    let level = levelsDB[e.target.level.value - 1];
    let lessonNumber = e.target.lessonNumber.value;
    let lessonId = new mongoose.Types.ObjectId();

    level.lessons.push(lessonId);
    const newLesson = {
      _id: lessonId,
      lessonNumber: lessonNumber,
      levelNumber: level.levelNumber,
    };
    await axios({
      method: "post",
      url: "/lesson",
      data: newLesson,
    }).then((res) => {
      asyncRequestLesson();
    });
    await axios({
      method: "put",
      url: `/level/${level._id}`,
      data: level,
    });
    setLoading(false);
  };
  const addNewTask = async (e) => {
    e.preventDefault();
    setLessonDlg(false);
    setLoading(true);
    let lesson = lessonsDB[e.target.lesson.value - 1];
    let taskNumber = e.target.taskNumber.value;
    let taskName = e.target.taskName.value;
    let taskId = new mongoose.Types.ObjectId();

    lesson.tasks.push(taskId);
    const newTask = {
      _id: taskId,
      number: Number(taskNumber),
      lesson: lesson._id,
      name: taskName,
    };
    await axios({
      method: "post",
      url: "/task",
      data: newTask,
    }).then((res) => {
      asyncRequestTasks();
    });
    await axios({
      method: "put",
      url: `/lesson/${lesson._id}`,
      data: lesson,
    });
    setLoading(false);
  };

  return (
    <div>
      <LoadingState className="z-20" open={loading} />
      <div className="grid h-screen place-items-center opacity-100 z-10 backdrop-blur-sm ">
        <div>
          <div className="w-full max-w-xl p-4 bg-white border border-gray-200 rounded-lg shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
            <form className="space-y-6" onSubmit={save}>
              <h5 className="text-xl font-medium text-gray-900 dark:text-white">
                Add a new question
              </h5>
              <div>
                <label
                  htmlFor="lesson"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Lesson
                </label>
                <div className="grid grid-col-2 grid-flow-col gap-4">
                  <select
                    id="lesson"
                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    required
                    onChange={(e) =>
                      setTasks(lessonsDB[e.target.selectedIndex - 1].tasks)
                    }
                  >
                    <option value="">Choose a Lesson Number</option>
                    {lessonsDB.map((lesson) => (
                      <option>{lesson.lessonNumber}</option>
                    ))}
                  </select>
                  <Fab
                    size="small"
                    color="primary"
                    aria-label="add"
                    onClick={(e) => setLessonDlg(true)}
                  >
                    <AddIcon />
                  </Fab>
                </div>
              </div>
              <div>
                <label
                  htmlFor="task"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Task
                </label>
                <div>
                  <div className="grid grid-col-2 grid-flow-col gap-4">
                    <select
                      id="task"
                      className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      required
                    >
                      <option value="">Choose a Task Number</option>
                      {tasks.map((task, i) => (
                        <option value={i}>{task.number}</option>
                      ))}
                    </select>
                    <Fab
                      size="small"
                      color="primary"
                      aria-label="add"
                      onClick={(e) => setTaskDlg(true)}
                    >
                      <AddIcon />
                    </Fab>
                  </div>
                </div>
              </div>

              <div>
                <label
                  htmlFor="question"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Question
                </label>
                <select
                  id="question"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                >
                  <option value="">Choose a Question Number</option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                </select>
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                  Instructions
                </label>
                <input
                  type="file"
                  id="file"
                  name="file"
                  accept=".mp3"
                  onChange={handleAudioChange}
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white ">
                  Images
                </label>
                <input
                  type="file"
                  id="file"
                  name="file"
                  accept="image/png, image/jpeg"
                  onChange={handleImgChange}
                />
              </div>
              <div>
                <label
                  for="video_url"
                  class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Video URL (If applicable)
                </label>
                <input
                  type="text"
                  id="video_url"
                  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Enter URL"
                />
              </div>
              {image.preview && (
                <div className="flex items-center justify-center ">
                  <img
                    src={image.preview}
                    width="100"
                    height="100"
                    className="border border-gray-200"
                    alt=""
                  />
                </div>
              )}
              <Button
                type="submit"
                class="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Save
              </Button>
            </form>
          </div>
        </div>
      </div>
      {/** Add lesson modal */}
      <Dialog open={lessonDlg}>
        <form onSubmit={addNewLesson}>
          <DialogTitle>New Lesson</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Add a new lesson the to the list
            </DialogContentText>

            <div>
              <label
                htmlFor="level"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Level
              </label>
              <select
                id="level"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
              >
                <option value="">Choose a Level Number</option>
                {levelsDB.map((level) => (
                  <option>{level.levelNumber}</option>
                ))}
              </select>
            </div>
            <TextField
              autoFocus
              required
              margin="dense"
              id="lessonNumber"
              label="Lesson Number"
              type="text"
              variant="standard"
              inputProps={{ inputMode: "numeric" }}
              helperText="Enter a number between 1 and 100"
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={(e) => setLessonDlg(false)}
              class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Cancel
            </Button>
            <Button
              type="submit"
              class=" text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
            >
              Save
            </Button>
          </DialogActions>
        </form>
      </Dialog>
      {/** Add task modal */}
      <Dialog open={taskDlg}>
        <form onSubmit={addNewTask}>
          <DialogTitle>New Lesson</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Add a new task the to the lesson list
            </DialogContentText>
            <div>
              <label
                htmlFor="level"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                lesson
              </label>
              <select
                id="lesson"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                required
              >
                <option value="">Choose a Lesson Number</option>
                {lessonsDB.map((lesson) => (
                  <option>{lesson.lessonNumber}</option>
                ))}
              </select>
            </div>
            <div>
              <TextField
                autoFocus
                required
                margin="dense"
                id="taskNumber"
                label="Task Number"
                type="number"
                variant="standard"
                inputProps={{ inputMode: "numeric", pattern: "[0-100]*" }}
                helperText="Enter a number between 1 and 100"
              />
            </div>
            <div>
              <TextField
                autoFocus
                margin="dense"
                id="taskName"
                label="Task Name"
                type="text"
                variant="standard"
              />
            </div>
          </DialogContent>

          <DialogActions>
            <Button
              onClick={(e) => setTaskDlg(false)}
              class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Cancel
            </Button>
            <Button
              type="submit"
              class=" text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
            >
              Save
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}

export default Admin;
