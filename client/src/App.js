import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  Link,
} from "react-router-dom";
import Home from "./components/Home";
import SoundIntroduction from "./components/Tasks/SoundIntroduction";
import SayItFast from "./components/Tasks/SayItFast";
import SoundsWriting from "./components/Tasks/SoundsWriting";
import Error from "./components/Error";
import Content from "./components/Content";
import LessonList from "./components/LessonList";
import Admin from "./components/Admin";
import TaskList from "./components/TaskList";
import readingKid from "./resources/reading-kid.jpg";
import logo from "./resources/logo.png";

function App() {
  return (
    <div>
      <img
        src={readingKid}
        class="flex block absolute top-1/2 left-1/2 max-w-full h-auto -translate-x-1/2 -translate-y-1/2 blur-md overflow-hidden"
        alt="background"
      />

      <Router>
        <div>
          <Link to="/home">
            <div>
              <img
                src={logo}
                alt="logo"
                className="z-10 absolute max-h-28 max-w-28 p-5"
              />
            </div>
          </Link>

          <Routes>
            <Route exact path="/home" element={<Home />} />
            <Route exact path="/" element={<Navigate to="/home" />} />
            <Route path="*" element={<Error />} />
            <Route path="/levels" element={<Content />} />
            <Route path="/:level/lessons" element={<LessonList />} />
            <Route path="/:level/:lesson/taskList" element={<TaskList />} />
            <Route
              path="/:level/:lesson/:task/Sounds Introduction"
              element={<SoundIntroduction />}
            />
            <Route
              path="/:level/:lesson/:task/Sounds Review"
              element={<SoundIntroduction />}
            />
            <Route
              path="/:level/:lesson/:task/Sounds Writing"
              element={<SoundsWriting />}
            />
            <Route
              path="/:level/:lesson/:task/Say it Fast"
              element={<SayItFast />}
            />
            <Route path="/admin" element={<Admin />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
